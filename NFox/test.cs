﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;



using NFox.Cad;


namespace NFox.Cad.Test
{

   

    /// <summary>
    /// 
    /// </summary>
    public class Test
    {
        /// <summary>
        /// 
        /// </summary>
        [CommandMethod("hello")]
        public void Hello()
        {


            var p = new Point3d(10, 10, 0);

            var f = OpFilter.Bulid(e => 
            !(e.Dxf(0) == "line" & e.Dxf(8) == "0") 
            | e.Dxf(0) != "circle" 
            & e.Dxf(8) == "2" 
            & e.Dxf(10) >= p);


            var f2 =
            OpFilter.Bulid(
                e =>
                    e.Or(
                    !e.And(e.Dxf(0) == "line", e.Dxf(8) == "0"),
                    
                    e.And(e.Dxf(0) != "circle", e.Dxf(8) == "2", e.Dxf(10) <= new Point3d(10, 10, 0)))
                );

            var fd =
            new OpOr
            {
                
                        !new OpAnd
                        {
                            { 0, "line" },
                            { 8, "0" },
                        },
                        new OpAnd
                        {
                            !new OpEqual(0, "circle"),
                            { 8, "2" },
                            { 10, new Point3d(10,10,0), ">,>,*" }
                        },
            };

            using (DBTransaction tr = new DBTransaction())
            {
                Line line1 = new Line(new Point3d(0, 0, 0), new Point3d(1, 1, 0));
                var item = line1.UpgradeOpenAndRun();

                Circle circle = new Circle(new Point3d(0, 0, 0), Vector3d.ZAxis, 10);
                var btr = tr.OpenCurrentSpace();
                tr.AddEntity(btr, line1, circle);
                tr.Editor.SelectAtPoint(new Point3d(0, 0, 0), OpFilter.Bulid(e => e.Dxf(10) == new Point3d(0, 0, 0)));
                tr.Editor.SelectAll(f2);

                var layer = tr.LayerTable.Add("1");
                tr.LayerTable.Remove("1");
                var layer1 = tr.LayoutDictionary;
                var btrr = tr.BlockTable.Add("2");
                tr.AddEntity(btrr, line1);
                tr.LinetypeTable.Add(
                    "hah",
                    ltt => 
                    {
                        ltt.AsciiDescription = "虚线";
                        ltt.PatternLength = 0.95; //线型的总长度
                        ltt.NumDashes = 4; //组成线型的笔画数目
                        ltt.SetDashLengthAt(0, 0.5); //0.5个单位的划线
                        ltt.SetDashLengthAt(1, -0.25); //0.25个单位的空格
                        ltt.SetDashLengthAt(2, 0); // 一个点
                        ltt.SetDashLengthAt(3, -0.25); //0.25个单位的空格
                    }); 
                tr.LinetypeTable.Add("hah", "虚线", 0.95, new double[] { 0.5, -0.25, 0, -0.25 });


                

                TypedValue[] acTypValAr = {
                    new TypedValue((int)DxfCode.Operator, "<or"),
                    new TypedValue((int)DxfCode.Start, "TEXT"),
                    new TypedValue((int)DxfCode.Start, "MTEXT"),
                    new TypedValue((int)DxfCode.Operator, "or>")
                };

            }
            var example = new { Greeting = "Hello", Name = "World" };
         

        }
        /// <summary>
        /// 
        /// </summary>
        [CommandMethod("helloblock")]
        public void HelloBlock()
        {
            using (DBTransaction tr = new DBTransaction())
            {
                Line line1 = new Line(new Point3d(0, 0, 0), new Point3d(1, 1, 0));
                Circle circle = new Circle(new Point3d(0, 0, 0), Vector3d.ZAxis, 10);
                //var id = tr.BlockTable.Add("test", new List<Entity> { line1, circle });

                //tr.AddEntity(tr.OpenCurrentSpace(), new BlockReference(new Point3d(0,0,0),id));

                //tr.InsertBlock(new Point3d(10, 10, 0), "test");
                AttributeDefinition def = new AttributeDefinition()
                {
                    Position = Point3d.Origin,
                    Tag = "xuhao",
                    Prompt = "xuhao",
                    TextStyleId = tr.TextStyleTable["Standard"],
                    Justify = AttachmentPoint.MiddleCenter,
                    AlignmentPoint = Point3d.Origin
                };

                var id = tr.AddBlock("test", new List<Entity> { line1, circle }, new List<AttributeDefinition> { def });
                var ttt = new Dictionary<string, string>
                {
                    { "xuhao", "123" }
                };
                tr.InsertBlock(Point3d.Origin, id, atts:ttt);
            }
        }
        /// <summary>
        /// Adms this instance.
        /// </summary>
        [CommandMethod("adm")]
        public void Adm()
        {
            using (DBTransaction tr = new DBTransaction())
            {
                tr.Editor.WriteMessage("starting...\n");
                var test = new RtfRangeNode
                {

                    {RtfSimpleNodeType.Alignment, "1" },
                    {RtfScriptNodeType.Fraction, "nihao","hello" },
                    {RtfSpecCodeNodeType.Newline },
                    {RtfLimitNodeType.Underline, "haha" },
                    {RtfSimpleNodeType.Color, "2" },
                    {RtfSimpleNodeType.Angle,"30" },
                    {"zheshisha" },
                    {RtfScriptNodeType.Tolerance,"","2" }
                 

                };
                MText mText = new MText();
                mText.Contents = test.Contents;

                var btr = tr.OpenCurrentSpace();
                tr.AddEntity(btr, mText);
                tr.Editor.WriteMessage("ending...\n");
            }
        }




        /// <summary>
        /// 测试函数
        /// </summary>
        public static void Test1()
        {

            _ = DBDictionaryEx.CreateDataTable(
                new Dictionary<string, CellType>
                {
                    { "Name", CellType.CharPtr },
                    { "Id", CellType.Integer },
                    { "Age", CellType.Integer },
                },
                new object[,]
                {
                    { "李四", 0, 12},
                    { "张三", 1, 14}
                });
        }
    }
}
